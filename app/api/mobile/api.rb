module Mobile
  class API < Grape::API
    prefix :api
    format :json
    formatter :json, Grape::Formatter::Rabl

    before do
      header['Access-Control-Allow-Origin'] = '*'
      header['Access-Control-Request-Method'] = '*'
      header['Access-Control-Allow-Methods'] = 'GET, POST, PUT, PATCH, OPTIONS, DELETE'
      header['Access-Control-Allow-Headers'] = 'true'
      #error!({message: 'Unauthorized. Invalid or expired token.'}, 401) unless authenticated || unauthenticated_request?

      # Escape the ampersand in the POST data.
      rack_input = env['rack.input'].gets
      # Rails.logger.error request.body.read

      if rack_input.present?
        rack_input = rack_input.gsub('&','%26')
        params_data = Rack::Utils.parse_query(rack_input, '&')
      else
        params_data = nil
      end

      request_data = {
        method: env['REQUEST_METHOD'],
        path:   env['PATH_INFO'],
        query:  env['QUERY_STRING'],
        params: params_data,
        #person_id: current_person.try(:id)
       }
      #ap request_data
      request_data
    end

    rescue_from ActiveRecord::RecordNotFound do |e|
      error_response(message: e.message, status: 404)
    end

    helpers do
      def warden
        env['warden']
      end

      def bearer_supported? bearer
        ['token'].include?(bearer)
      end

      def authenticated
        return true if warden.authenticated?
        return false unless request.env['HTTP_AUTHORIZATION']
        authentication = request.env['HTTP_AUTHORIZATION'].split(' ')
        bearer = authentication[0]
        key = authentication[1]
        bearer_supported?(bearer) && @person = User.find_by_authentication_token(key)
       # Rails.logger.fatal(bearer)
       # Rails.logger.fatal(key)
     end
   end
     mount Mobile::V1::API
 end
end
