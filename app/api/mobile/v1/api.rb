module Mobile
  module V1
    class API < Grape::API
       mount Mobile::V1::Users
       mount Mobile::V1::Merchants
      # mount Mobile::V1::Promotions
      # mount Mobile::V1::Profile
      # mount Mobile::V1::Search
      # mount Mobile::V1::Devices
      # mount Mobile::V1::Bday
    end
  end
end
