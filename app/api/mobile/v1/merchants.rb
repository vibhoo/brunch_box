module Mobile
  module V1
    class Merchants < Grape::API
      version :v1

      helpers do

      end

      resource :merchants do
        desc 'List of merchants'
        params do
          optional :flag, type: Integer
        end
        get '/', rabl: 'merchants/index' do
        @merchants = Merchant.all
        end#do
        desc 'List of merchants'
        params do
          requires :id, type: Integer
        end

        get '/:id', rabl: 'merchants/show' do
          @merchant   = Merchant.find(params[:id])
        end#do
      end#resource
    end#class
  end#module v1
end#module mobile
