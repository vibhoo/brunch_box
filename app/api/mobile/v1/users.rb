#require 'action_controller/metal/strong_parameters'
module Mobile
  module V1
    class Users < Grape::API
      version :v1

      helpers do
        # def item_params
        #   ActionController::Parameters.new(params).require(:item).permit(:attribute)
        # end
        def user_params
          declared(params, include_missing: false)
        end

        def user_params_sanitized
          secure_user_params = user_params[:user]
          secure_user_params
        end
      end #helper do

      resource :users do
        desc 'User Profile'
        params do
          requires :user, type: Hash do
            requires :name, type: String, desc: 'name'
            requires :email, type: String, desc: 'email'
            requires :phone, type: String, desc: 'phone'
          end
        end
        post '/', rabl: 'users/create' do
          @user = User.find_by_name(params[:user][:name])
          if @user
            @user.update_attributes(:name =>  params[:user][:name], :email => params[:user][:email], :phone => params[:user][:phone])
          else
            @user = User.new(user_params_sanitized)
            unless @user.save
              error!({message: @user.errors.messages}, 422)
            end #unless
          end #if
        end # do
      end #resource
    end
  end
end
