class Category < ActiveRecord::Base
  belongs_to :merchant
  has_many :items
end
