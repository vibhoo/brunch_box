object @merchants => :merchants
attributes :id, :name, :cuisines, :location, :lat, :lon
