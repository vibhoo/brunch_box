object @merchant => :merchant
attributes :id, :name, :location, :lat, :long

# child(@categories) {attributes :id, :name}
#
# child(@items) {attributes :id, :name, :category_id}

child :categories do
  attributes :id, :name
  child :items do
    attributes :id, :name
    node (:price) {|m| m.price.first(3).gsub("\n", "")}
  end
end
