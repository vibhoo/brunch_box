class CreateMerchants < ActiveRecord::Migration
  def change
    create_table :merchants do |t|
      t.string :name
      t.string :location
      t.string :cuisines
      t.string :link
      t.float :rating

      t.timestamps
    end
  end
end
