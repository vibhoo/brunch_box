class AddLatAndLongToMerchant < ActiveRecord::Migration
  def change
    add_column :merchants, :lat, :string
    add_column :merchants, :lon, :string
  end
end
