class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.references :user, index: true
      t.text :located_at
      t.string :landmark
      t.text :area

      t.timestamps
    end
  end
end
