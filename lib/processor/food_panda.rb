module Processor
	module FoodPanda
		def self.process params
      response = ''
			url = 'https://www.foodpanda.in/restaurants/city/lucknow'
			html_data = HttpProcessor.process url
			if html_data.eql? 'error'
				"Sorry, Something went wrong"
			else
				doc = Nokogiri::HTML( html_data )
        results = doc.css('.vendor__details').first(10)
				results.each do |item|
					merchant = item.search('.vendor__title').each do |i|
						if i.text.strip == "Dominos"
							break
						else
							link   = i.search('a').map { |e| e[:href]  }
							cuisines =  item.search(".vendor__cuisines li").map { |e| e  }

							Merchant.create(name: i.text.strip, cuisines: cuisines.join(", "), link: "https://www.foodpanda.in"+link[0])
							sleep(5)
							process_link link[0], Merchant.last.id
							response << "#{i.text.strip} \n #{cuisines}"
						end
					end
				end
        response
      end
    end

		def self.process_link link, merchant_id
			#url = "https://www.foodpanda.in/restaurant/n7kc/saffron-kitchen-1"
			url = "https://www.foodpanda.in"+link
			html_data = HttpProcessor.process url
			if html_data.eql? 'error'
				"Sorry, Something went wrong"
			else
				doc = Nokogiri::HTML( html_data )
				results = doc.search('#menu a').first(10)
				results.each do |item|
					if item.present?
						Category.create(merchant_id: merchant_id, name: item.text.strip)
						process_item url + item[:href], Category.last.id
					end
				end
			end
		end

		def self.process_item url, category_id
			response  = ''
			html_data = HttpProcessor.process url
			if html_data.eql? 'error'
				"Sorry, Something went wrong"
			else
				doc   = Nokogiri::HTML(html_data)
				results = doc.search('.menu-item').first(15)
				results.each do |i|
					if i.present?
						name  = i.search(".menu-item__title").text.strip
						price = i.search(".menu-item__variation__price").text.strip
						Item.create(category_id:  category_id, name: name, price: price.gsub("Rs.", ""))
					end
				end
				# response << "#{name} \n #{price}"
				# response
			end
		end
  end
end
